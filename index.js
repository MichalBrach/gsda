const express = require("express");
const app = express();
const path = require('path');

app.set("view engine", "ejs")
app.set('views', path.join(__dirname, '/views'))

app.get("/", (req, res) => {
    res.render("home.ejs")
})

app.get("/items-synchronization", (req, res) => {
    res.render("itemsSynchronization.ejs")
})

app.get("/manager-dashboard", (req, res) => {
    res.render("managerDashboard.ejs")
})

app.get("/daily-meetings", (req, res) => {
    res.render("dailyMeetings.ejs")
})

app.get("/notifications", (req, res) => {
    res.render("notifications.ejs")
})

app.get("/chat", (req, res) => {
    res.render("chat.ejs")
})

app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
  })

app.listen("8080", () => {
    console.log("Listening on port 8080")
})